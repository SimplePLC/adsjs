export const schema = Types => `
${Types}
type PlcConn {
    amsNetId: String!
    port: Int!
}
type PLC {
    ams_address: PlcConn!
    config_version: Int!
}
type Program {
	standard: String,
	complex: String,
	building: String,
}
input IProgram {
	standard: String,
	complex: String,
	building: String,
}

type Query {
  ST_ADS_TO_BC(conn: String!): ST_ADS_TO_BC
  ST_ADS_FROM_BC(conn: String!): ST_ADS_FROM_BC
  ST_RETAIN_DATA(conn: String!): ST_RETAIN_DATA
  listPLC: [PLC]!
  aMerker(conn: String!, index: Int!): Int
  Program(conn: String!): Program
}
type Mutation {
  ST_ADS_TO_BC(conn: String!, plc: I_ST_ADS_TO_BC): ST_ADS_TO_BC
  ST_RETAIN_DATA(conn: String!, plc: I_ST_RETAIN_DATA): ST_RETAIN_DATA
  aMerker(conn: String!, index: Int!, value: Int!): Int
  Program(conn: String!, data: IProgram): Program
}
type Subscription {
  ST_ADS_TO_BC(conn: String!): ST_ADS_TO_BC
  ST_ADS_FROM_BC(conn: String!): ST_ADS_FROM_BC
  ST_RETAIN_DATA(conn: String!): ST_RETAIN_DATA
  aMerker(conn: String!, index: Int!): Int
}
  `;
