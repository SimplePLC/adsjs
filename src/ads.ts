import { connect, createServer, Socket } from "net";

function readPacket(socket: Socket, readLength = 0) {
    return new Promise(res => {
        let buf = Buffer.alloc(0);
        socket.on("data", (data: Buffer) => {
            buf = Buffer.concat([buf, data]);
            if (buf.length >= readLength) {
                res(buf);
            }
        });
    });
}
function handleResponse(packet) {
    return packet.slice(6 + 32);
}
function handleReadRes(packet) {
    return handleResponse(packet).slice(8);
}
function handleWriteRes(packet) {
    return handleResponse(packet);
}

function netIdToString(buffer) {
    const netIdArr = [...buffer.slice(0, 6)];
    const netId = netIdArr.join(".");
    return netId + ":" + buffer.readUInt16LE(6);
}
function handleWriteReq(packet) {
    const target = packet.slice(6, 14);
    const source = packet.slice(14, 22);
    const commandId = packet.slice(22, 24);
    const invokeId = packet.slice(34, 38);
    const data = packet.slice(50);
    const res = Buffer.alloc(42);
    invokeId.copy(res, 34);
    commandId.copy(res, 22);
    target.copy(res, 14);
    source.copy(res, 6);
    res.writeUInt16LE(5, 24);
    res.writeUInt32LE(36, 2);

    return {
        commandId: commandId.readUInt16LE(),
        data,
        invokeId: invokeId.readUInt32LE(),
        res,
        source: netIdToString(source),
        target: netIdToString(target)
    };
}

function createAmsHeader(buffer) {
    const header = Buffer.alloc(6);
    header.writeUInt32LE(buffer.length, 2);
    return Buffer.concat([header, buffer]);
}

function createAdsHeader({ target, source, invokeId, cId }, buffer) {
    const rest = Buffer.alloc(16);
    rest.writeUInt16LE(cId, 0);
    rest.writeUInt16LE(4, 2);
    rest.writeUInt32LE(buffer.length, 4);
    rest.writeUInt32LE(invokeId, 12);
    return Buffer.concat([target, source, rest, buffer]);
}

function createWriteReq(opts, indexGrp, indexOffs, data, invokeId) {
    const reqMeta = Buffer.alloc(12);
    reqMeta.writeUInt32LE(indexGrp, 0);
    reqMeta.writeUInt32LE(indexOffs, 4);
    reqMeta.writeUInt32LE(data.length, 8);
    const req = Buffer.concat([reqMeta, Buffer.from(data)]);
    opts.invokeId = invokeId;
    opts.cId = 3;
    return createAmsHeader(createAdsHeader(opts, req));
}

function createReadReq(opts, indexGrp, indexOffs, length, invokeId) {
    const reqMeta = Buffer.alloc(12);
    reqMeta.writeUInt32LE(indexGrp, 0);
    reqMeta.writeUInt32LE(indexOffs, 4);
    reqMeta.writeUInt32LE(length, 8);
    opts.invokeId = invokeId;
    opts.cId = 2;
    return createAmsHeader(createAdsHeader(opts, reqMeta));
}

function amsConnToBuffer(conn) {
    const net = Buffer.from(conn.amsNetId.split(".").map(e => parseInt(e, 10)));
    const port = Buffer.alloc(2);
    port.writeUInt16LE(conn.port, 0);
    return Buffer.concat([net, port]);
}

export class Client {
    private target: Buffer;
    private source: Buffer;
    private ip: any;
    private invId: number;
    private queue: Array<[Buffer, (buf: any) => any, number]>;
    private client: Socket;
    constructor(target, source, ip) {
        this.target = amsConnToBuffer(target);
        this.source = amsConnToBuffer(source);
        this.ip = ip.split(":").reverse();
        this.queue = [];
        if (this.ip.length < 2) {
            this.ip.push(48898);
        }
        this.invId = 0;
        this.send = this.send.bind(this);
        setTimeout(this.send, 100);
    }
    public async read(idxGrp, idxOffs, length) {
        const req = createReadReq(
            {
                source: this.source,
                target: this.target
            },
            idxGrp,
            idxOffs,
            length,
            this.invId++
        );
        return handleReadRes(await this.addToQueue(req, 6 + 32 + 8 + length));
    }

    public async write(idxGrp, idxOffs, data) {
        const req = createWriteReq(
            {
                source: this.source,
                target: this.target
            },
            idxGrp,
            idxOffs,
            data,
            this.invId++
        );
        return handleWriteRes(await this.addToQueue(req));
    }
    private addToQueue(buf: Buffer, resLength = 0) {
        return new Promise(res => {
            this.queue.push([buf, res, resLength]);
        });
    }
    private async send() {
        if (this.ip && this.queue.length > 0) {
            if (!this.client) {
                this.client = connect(this.ip[0], this.ip[1]);
                // this.client.setNoDelay(true);
                // tslint:disable-next-line
                this.client.on("error", () => {});
            }

            for (const [buf, res, length] of this.queue) {
                const p = readPacket(this.client, length);
                this.client.write(buf);
                const result = await p;
                res(result);
            }

            this.queue = [];
            this.client.destroy();
            this.client = null;
            setTimeout(this.send, 100);
        } else {
            setTimeout(this.send, 100);
        }
    }
}

export function createAdsServer(callback) {
    return createServer(socket => {
        socket.on("data", data => {
            const res = handleWriteReq(data);
            socket.write(res.res);
            delete res.res;
            callback(res);
        });
        socket.on("error", err => console.error(err));
    }).listen(48898);
}
