import { pubsub, mapOfPlc, mapOfClients, mapOfConfVersion } from "./index";
import {plcConfig, getConfig} from "./config";
import { AdsConnToString } from "./handleConfig";
import { writeFileSync } from "fs";

function initMaster(conn: string) {
    const client = mapOfClients.get(conn);
    const confVersion = mapOfConfVersion.get(conn);
    const conf = getConfig(confVersion);
    const res = mapOfPlc.get(conn);
    res.toBC.udiDataSize = conf.ST_ADS_TO_BC.byteLength;
    res.toBC.udiVersion = confVersion;
    res.toBC.udiIdxGrp = 0x4020;
    res.toBC.udiIdxOffs = 0;
    res.toBC.sNetId = plcConfig.ams_source.amsNetId;
    res.toBC.uiPort = plcConfig.ams_source.port;
    res.toBC.dtDateTime = Math.round(Date.now() / 1000);
    res.toBC.tDelayBetweenAdsTelegrams = 100;
    res.toBC.tWatchdog = 10000;
    res.toBC.bReadMerkerCyclic = true;
    const buf = conf.ST_ADS_TO_BC.fromObject(res.toBC);
    client.write(0x4020, 0, buf);
    setInterval(cyclicHeartbeat(conn), 5000);
    setInterval(cyclicReadRetainData(conn), 5000);
}
function cyclicHeartbeat(conn: string) {
    const confVersion = mapOfConfVersion.get(conn);
    const client = mapOfClients.get(conn);
    const conf = getConfig(confVersion);
    const buf = Buffer.alloc(4);
    const offs = conf.ST_ADS_TO_BC.offsetOffKey("udiRequestCounter");
    return () => {
        const res = mapOfPlc.get(conn);
        res.toBC.udiRequestCounter++;
        mapOfPlc.set(conn, res);
        buf.writeUInt32LE(res.toBC.udiRequestCounter, 0);
        client.write(0x4020, offs, buf);
    };
}

function cyclicReadRetainData(conn: string) {
    const confVersion = mapOfConfVersion.get(conn);
    const client = mapOfClients.get(conn);
    const conf = getConfig(confVersion);
    return async () => {
        const res = mapOfPlc.get(conn);
        const data = await client.read(
            0x4020,
            1048,
            conf.ST_RETAIN_DATA.byteLength
        );
        const newRetain = conf.ST_RETAIN_DATA.asObject(data);
        if (JSON.stringify(newRetain) !== JSON.stringify(res.retain)) {
            res.retain = newRetain;
            mapOfPlc.set(conn, res);
            pubsub.publish(conn + "ST_RETAIN_DATA", {
                ST_RETAIN_DATA: res.retain
            });
        }
    };
}

export function init() {
    plcConfig.plcs.map(v => {
        initMaster(AdsConnToString(v.ams_address));
    });
}
