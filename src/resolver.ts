import { mapOfPlc, mapOfConfVersion, mapOfClients, pubsub } from "./index";
import { plcConfig, getConfig } from "./config";
// import * as fs from "fs";
import * as fse from "fs-extra";

function merge(obj1, obj2) {
    if (Array.isArray(obj1)) {
        const maxIndex = obj1.length < obj2.length ? obj1.length : obj2.length;
        for (let i = 0; i < maxIndex; i++) {
            if (typeof obj1[i] === "object") {
                if (obj2[i] !== undefined) {
                    merge(obj1[i], obj2[i]);
                }
            } else {
                if (obj2[i] !== undefined) {
                    obj1[i] = obj2[i];
                }
            }
        }
    } else if (typeof obj1 === "object") {
        for (const [key, value] of Object.entries(obj2)) {
            if (obj1.hasOwnProperty(key) && key !== "__typename") {
                if (typeof value === "object") {
                    merge(obj1[key], obj2[key]);
                } else {
                    obj1[key] = obj2[key];
                }
            }
        }
    } else {
        console.error("cant use by reference on non object types");
    }
}

const getConfigAndClient = conn => {
    const plcStruct = mapOfPlc.get(conn);
    const client = mapOfClients.get(conn);
    const conf = getConfig(mapOfConfVersion.get(conn));
    return { plcStruct, client, conf };
};

const checkAMerker = (old, n, conn) => {
    for (let i = 0; i < n.length; i++) {
        if (old[i] !== n[i]) {
            pubsub.publish(conn + "aMerker." + i, {
                aMerker: n[i]
            });
        }
    }
};
const handleRetainChange = (conn) => {
    const { client, plcStruct, conf } = getConfigAndClient(conn);
    const copy = {
        ...plcStruct.toBC,
        bSetNewRetainData: true,
        uiLengthNewRetainData: conf.ST_ADS_TO_BC.byteLength,
        uiFirstNewRetainByte: 0
    };
    client.write(0x4020, 0, conf.ST_ADS_TO_BC.fromObject(copy));
}

const handleRetainDataUpdate = (conn, copy) => {
    const { client, plcStruct, conf } = getConfigAndClient(conn);
    
    const newBin = conf.ST_RETAIN_DATA.fromObject(copy);
    client.write(0x4020, 1048, newBin);
    checkAMerker(plcStruct.retain.aMerker, copy.aMerker, conn);
    plcStruct.retain = copy;
    mapOfPlc.set(conn, plcStruct);
    pubsub.publish(conn + "ST_RETAIN_DATA", {
        ST_RETAIN_DATA: copy
    });
    handleRetainChange(conn);
};

export const resolvers = () => {
    return {
        Mutation: {
            ST_ADS_TO_BC(parent, { plc, conn }, context, info) {
                const { client, plcStruct, conf } = getConfigAndClient(conn);
                const copy = JSON.parse(JSON.stringify(plcStruct.toBC));
                merge(copy, plc);
                client.write(0x4020, 0, conf.ST_ADS_TO_BC.fromObject(copy));
                pubsub.publish(conn + "ST_ADS_TO_BC", {
                    ST_ADS_TO_BC: copy
                });
                plcStruct.toBC = copy;
                mapOfPlc.set(conn, plcStruct);
                return copy;
            },
            ST_RETAIN_DATA(parent, args, context, info) {
                const { plc, conn } = args;
                const { plcStruct } = getConfigAndClient(conn);
                const copy = JSON.parse(JSON.stringify(plcStruct.retain));
                merge(copy, plc);
                handleRetainDataUpdate(conn, copy);
                return copy;
            },
            aMerker(parent, { conn, index, value }, context, info) {
                const { client, plcStruct, conf } = getConfigAndClient(conn);
                if (plcStruct) {
                    const offs = conf.ST_RETAIN_DATA.offsetOffKey("aMerker");
                    const data = Buffer.alloc(2);
                    data.writeUInt16LE(value, 0);
                    client.write(0x4020, 1048 + offs + index * 2, data);
                    plcStruct.retain.aMerker[index] = value;
                    pubsub.publish(conn + "aMerker." + index, {
                        aMerker: value
                    });
                    pubsub.publish(conn + "ST_RETAIN_DATA", {
                        ST_RETAIN_DATA: plcStruct.retain
                    });
                    return value;
                } else {
                    return null;
                }
            },
            Program(parent, { conn, data }, context, info) {
                const d = { ...data };
                d.standard = JSON.parse(d.standard);
                d.complex = JSON.parse(d.complex);
                d.building = JSON.parse(d.building);
                fse.outputJson(`./programs/${conn}.json`, d);
                return data;
            }
        },
        Query: {
            listPLC(parent, args, context, info) {
                return plcConfig.plcs;
            },
            ST_ADS_TO_BC(parent, { conn }, context, info) {
                const res = mapOfPlc.get(conn);
                if (res) {
                    return res.toBC;
                } else {
                    return null;
                }
            },
            ST_ADS_FROM_BC(parent, { conn }, context, info) {
                const res = mapOfPlc.get(conn);
                if (res) {
                    return res.fromBC;
                } else {
                    return null;
                }
            },
            ST_RETAIN_DATA(parent, { conn }, context, info) {
                const res = mapOfPlc.get(conn);
                if (res) {
                    return res.retain;
                } else {
                    return null;
                }
            },
            aMerker(parent, { conn, index }, context, info) {
                const res = mapOfPlc.get(conn);
                if (res) {
                    return res.retain.aMerker[index];
                } else {
                    return null;
                }
            },
            Program(parent, { conn }, context, info) {
                try {
                    const { standard, complex, building } = fse.readJsonSync(
                        `./programs/${conn}.json`
                    );
                    const data = { standard, complex, building };
                    data.standard = JSON.stringify(standard);
                    data.complex = JSON.stringify(complex);
                    data.building = JSON.stringify(building);
                    return data;
                } catch (e) {
                    return null;
                }
            }
        },
        Subscription: {
            ST_ADS_FROM_BC: {
                subscribe: (parent, { conn }, context, info) =>
                    pubsub.asyncIterator(conn + "ST_ADS_FROM_BC")
            },
            ST_ADS_TO_BC: {
                subscribe: (parent, { conn }, context, info) =>
                    pubsub.asyncIterator(conn + "ST_ADS_TO_BC")
            },
            ST_RETAIN_DATA: {
                subscribe: (parent, { conn }, context, info) =>
                    pubsub.asyncIterator(conn + "ST_RETAIN_DATA")
            },
            aMerker: {
                subscribe: (parent, { conn, index }, context, info) =>
                    pubsub.asyncIterator(conn + "aMerker." + index)
            }
        }
    };
};
