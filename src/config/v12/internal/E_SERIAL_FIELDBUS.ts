import * as iecstruct from "../../iecstruct";

enum SERIAL_FIELDBUS {
    eNoFieldBus = 0,
    eDMX = 1,
    eModbusRtuMAster = 2,
    eModbusRtuSlave = 3
}

export const E_SERIAL_FIELDBUS = new iecstruct.ENUM(SERIAL_FIELDBUS);
