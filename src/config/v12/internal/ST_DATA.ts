import { BYTE, STRUCT } from "../../iecstruct";

export let ST_DATA = new STRUCT("ST_DATA")
    .addElement("bySourceType", new BYTE())
    .addElement("byChannel", new BYTE());
