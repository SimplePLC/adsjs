import { INT, STRUCT, UDINT, UINT, WORD } from "../../iecstruct";
import { ST_PARAMETER_BLIND_POSITION_TABLE } from "./ST_PARAMETER_BLIND_POSITION_TABLE";

export const ST_PARAMETER_BLIND = new STRUCT("ST_PARAMETER_BLIND")
    .addElement("udiPosIntervall", new UDINT())
    .addElement("wBacklashTimeUp", new WORD())
    .addElement("wBacklashTimeDown", new WORD())
    .addElement("wTurningTimeUp", new WORD())
    .addElement("wTurningTimeDown", new WORD())
    .addElement("wBlindHeight", new WORD())
    .addElement("wManualBlockTime", new WORD())
    .addElement("iAngleLimitUp", new INT())
    .addElement("iAngleLimitDown", new INT())
    .addElement("uiLouvreWidth", new UINT())
    .addElement("uiLouvreSpacing", new UINT())
    .addElement("uiFixedShutterHeight", new UINT())
    .addElement("uiMaxLightIncidence", new UINT())
    .addElement("uiWindowHeight", new UINT())
    .addElement("uiWindowDistanceFloor", new UINT())
    .addElement("stBlindPositionTable", ST_PARAMETER_BLIND_POSITION_TABLE)
    .addElement("ePositioningMode", new INT());
