import * as iecstruct from "../../iecstruct";
import * as config from "../global_config";

export const ST_COUPLER_CONFIG = new iecstruct.STRUCT("ST_COUPLER_CONFIG")
    .addElement("aIpAddress", new iecstruct.ARRAY(4, new iecstruct.BYTE()))
    .addElement("aSubnetMask", new iecstruct.ARRAY(4, new iecstruct.BYTE()))
    .addElement("sName", new iecstruct.STRING(config.gMaxLengthOfName))
    .addElement("sMac", new iecstruct.STRING(6))
    .addElement("byDummy", new iecstruct.BYTE());
