import { BOOL, STRUCT, WORD } from "../../iecstruct";

export let ST_TERMINAL_DESCRIPTION = new STRUCT("ST_TERMINAL_DESCRIPTION")
    .addElement("wName", new WORD())
    .addElement("wInputBits", new WORD())
    .addElement("wOutputBits", new WORD())
    .addElement("wChannels", new WORD())
    .addElement("bInput", new BOOL())
    .addElement("bOutput", new BOOL())
    .addElement("wDummy", new WORD());
