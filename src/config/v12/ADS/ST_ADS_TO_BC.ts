import * as iecstruct from "../../iecstruct";
import * as config from "../global_config";
import { ST_COUPLER_CONFIG } from "../internal/ST_COUPLER_CONFIG";
import { ST_REG_DATA } from "../internal/ST_REG_DATA";
import { ST_TERMINAL_DESCRIPTION } from "../internal/ST_TERMINAL_DESCRIPTION";

export const ST_ADS_TO_BC = new iecstruct.STRUCT("ST_ADS_TO_BC")
    .addElement("udiDataSize", new iecstruct.UDINT())
    .addElement("udiVersion", new iecstruct.UDINT())
    .addElement("udiIdxGrp", new iecstruct.UDINT())
    .addElement("udiIdxOffs", new iecstruct.UDINT())
    .addElement("udiRequestCounter", new iecstruct.UDINT()) // 20
    .addElement("sNetId", new iecstruct.STRING(23)) // 44
    .addElement("uiPort", new iecstruct.UINT()) // 46
    .addElement("Outputs", new iecstruct.ARRAY(512, new iecstruct.BYTE())) // 558
    .addElement("bUnchangedOutputsWatchdog", new iecstruct.BOOL())
    .addElement("bReadMerkerCyclic", new iecstruct.BOOL())
    .addElement("dtDateTime", new iecstruct.DT())
    .addElement("tWatchdog", new iecstruct.TIME())
    .addElement("tDelayBetweenAdsTelegrams", new iecstruct.TIME()) // 572
    .addElement("tRefreshAnalog", new iecstruct.TIME()) // 576
    .addElement(
        "stTerminalDescription",
        new iecstruct.ARRAY(15, ST_TERMINAL_DESCRIPTION)
    )
    .addElement("bGetAllDataFromBc", new iecstruct.BOOL())
    .addElement("bPersistentDataActive", new iecstruct.BOOL())
    .addElement("bResetPersistentData", new iecstruct.BOOL())
    .addElement("bRunBcLogic", new iecstruct.BOOL())
    .addElement("bRunBcLogicWatchdog", new iecstruct.BOOL())
    .addElement("bAnalyseSensorErrors", new iecstruct.BOOL())
    .addElement("bSetAllOutputsFalse", new iecstruct.BOOL())
    .addElement("bRebuildMaping", new iecstruct.BOOL())
    .addElement("bReboot", new iecstruct.BOOL())
    .addElement("bSetNewRetainData", new iecstruct.BOOL())
    .addElement("uiLengthNewRetainData", new iecstruct.UINT())
    .addElement("uiFirstNewRetainByte", new iecstruct.UINT())
    .addElement("bReadMerker", new iecstruct.BOOL())
    .addElement("bSendNoInputChangesToMaster", new iecstruct.BOOL())
    .addElement(
        "stAnalogChannelRegs",
        new iecstruct.ARRAY(
            config.uiMaxNoOfAnalogChannels,
            new iecstruct.ARRAY(
                config.uiMaxNoOfPersistentAnalogReg + 1,
                ST_REG_DATA
            )
        )
    )
    .addElement("uiNoOfDmxChannelsToAnalogChannels", new iecstruct.UINT())
    .addElement("iLongitude", new iecstruct.INT())
    .addElement("iLatitude", new iecstruct.INT())
    .addElement("iTimeZoneMinute", new iecstruct.INT())
    .addElement("sSecondMasterNetId", new iecstruct.STRING(23))
    .addElement("udiSecondMasterPort", new iecstruct.UINT())
    .addElement("wPin", new iecstruct.WORD())
    .addElement("tWatchdogSecondMaster", new iecstruct.TIME())
    .addElement("sLicenseKey", new iecstruct.STRING(15))
    .addElement("wDummy3", new iecstruct.WORD())
    .addElement("stCouplerConfig", ST_COUPLER_CONFIG);