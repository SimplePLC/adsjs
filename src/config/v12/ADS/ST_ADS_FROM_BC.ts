import * as iecstruct from "../../iecstruct";
import * as config from "../global_config";
import { E_SERIAL_FIELDBUS } from "../internal/E_SERIAL_FIELDBUS";
import { ST_COUPLER_CONFIG } from "../internal/ST_COUPLER_CONFIG";

export const ST_ADS_FROM_BC = new iecstruct.STRUCT("ST_ADS_FROM_BC")
    .addElement("udiDataSize", new iecstruct.UDINT())
    .addElement("udiVersion", new iecstruct.UDINT())
    .addElement("udiResponseCounter", new iecstruct.UDINT())
    .addElement("dtDateTime", new iecstruct.DT())
    .addElement("Inputs", new iecstruct.ARRAY(512, new iecstruct.BYTE()))
    .addElement("sError", new iecstruct.STRING(255))
    .addElement("udiTerminalConfigChangeCounter", new iecstruct.UDINT())
    .addElement("aTerminals", new iecstruct.ARRAY(64, new iecstruct.WORD()))
    .addElement("eActiveSerialFieldbus", E_SERIAL_FIELDBUS)
    .addElement("udiPersistentDataAddr", new iecstruct.UINT())
    .addElement("uiLastInputByte", new iecstruct.UINT())
    .addElement("uiLastOutputByte", new iecstruct.UINT())
    .addElement("uiKBusCode", new iecstruct.UINT())
    .addElement("uiKBusArgument", new iecstruct.UINT())
    .addElement("uiCycleTimeMs", new iecstruct.UINT())
    .addElement("uiTerminalDescriptionFailed", new iecstruct.UINT())
    .addElement("uiLastNoOfAnalogChannel", new iecstruct.UINT())
    .addElement("uiLastNoOfDigitalChannel", new iecstruct.UINT())
    .addElement("uiLastNoOfComTerminal", new iecstruct.UINT())
    .addElement("uiLastNoOfMeasurementChannel", new iecstruct.UINT())
    .addElement("uiFirstFieldbusAnalogChannel", new iecstruct.UINT())
    .addElement("bPersistentDataActive", new iecstruct.BOOL())
    .addElement("bBcLogicActive", new iecstruct.BOOL())
    .addElement("bMerkerRefreshed", new iecstruct.BOOL())
    .addElement("bSecondMasterActive", new iecstruct.BOOL())
    .addElement("sSystemId", new iecstruct.STRING(31))
    .addElement("wDummy", new iecstruct.WORD())
    .addElement("stCouplerConfig", ST_COUPLER_CONFIG)
    .addElement(
        "aMerker",
        new iecstruct.ARRAY(config.uiMaxNoOfRetainMerker, new iecstruct.WORD())
    );
