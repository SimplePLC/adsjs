module.exports = {
  "uiMaxNoOfAnalogChannels": 24,
  "uiMaxNoOfDigitalChannels": 64,
  "uiMaxNoOfComTerminals": 10,
  "uiMaxNoOfPersistentAnalogReg": 1,
  "uiMaxNoOfMeasurementChannels": 9,
  "uiMaxNoOfMeasurementValues": 16,
  "uiMaxNoOfStandardLogics": 64,
  "uiMaxNoOfStandardLogicInputs": 3,
  "uiMaxNoOfStandardLogicOutputs": 1,
  "uiMaxNoOfComplexLogics": 16,
  "uiMaxNoOfComplexLogicInputs": 6,
  "uiMaxNoOfComplexLogicOutputs": 3,
  "uiMaxNoOfBuildingLogics": 16,
  "uiMaxNoOfBuildingLogicInputs": 6,
  "uiMaxNoOfBuildingLogicOutputs": 4,
  "uiMaxNoOfRetainMerker": 256,
  "dtDateTimeMin": 60,
  "dtDateTime7Days": 518400,
  "gMaxLengthOfName": 9,
  "stDefaultParameter": {
    "udiBCPort": 800,
    "udiBCIdxGrp": 16416,
    "udiBCIdxOffs": 0,
    "stTerminalDescription": [
      {
        "wName": 6001,
        "wInputBits": 40,
        "wOutputBits": 40,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6011,
        "wInputBits": 40,
        "wOutputBits": 40,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6021,
        "wInputBits": 64,
        "wOutputBits": 64,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6031,
        "wInputBits": 192,
        "wOutputBits": 192,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6041,
        "wInputBits": 192,
        "wOutputBits": 192,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6301,
        "wInputBits": 192,
        "wOutputBits": 192,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6401,
        "wInputBits": 288,
        "wOutputBits": 288,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6771,
        "wInputBits": 96,
        "wOutputBits": 96,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6781,
        "wInputBits": 192,
        "wOutputBits": 192,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6811,
        "wInputBits": 32,
        "wOutputBits": 32,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6821,
        "wInputBits": 40,
        "wOutputBits": 40,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6831,
        "wInputBits": 40,
        "wOutputBits": 40,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6841,
        "wInputBits": 40,
        "wOutputBits": 40,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      },
      {
        "wName": 6581,
        "wInputBits": 96,
        "wOutputBits": 96,
        "wChannels": 1,
        "bInput": true,
        "bOutput": true,
        "wDummy": 0
      }
    ]
  },
  "HEXASC_CODES": [
    48,
    49,
    50,
    51,
    52,
    53,
    54,
    55,
    56,
    57,
    65,
    66,
    67,
    68,
    69,
    70
  ]
};
