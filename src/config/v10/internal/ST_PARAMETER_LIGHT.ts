import { INT, STRUCT, WORD } from "../../iecstruct";

export const ST_PARAMETER_LIGHT = new STRUCT("ST_PARAMETER_LIGHT")
    .addElement("iMin", new INT())
    .addElement("iMax", new INT())
    .addElement("iMinMemMode", new INT())
    .addElement("iOnValueNoMemMode", new INT())
    .addElement("wDimmRampTime", new WORD())
    .addElement("wMinMaxRampDelay", new WORD())
    .addElement("wDimmOffTime", new WORD())
    .addElement("wDimmOnTime", new WORD());
