import { ARRAY, BOOL, BYTE, STRUCT, UINT } from "../../iecstruct";
/*	uiSunElevation		: ARRAY[0..5] OF UINT; (* Degree * 100 *)
	uiBlindPosition		: ARRAY[0..5] OF UINT;

	(*Valid set*)
	bValid				: BOOL;
	byDummy				: BYTE;
	*/
export const ST_PARAMETER_BLIND_POSITION_TABLE = new STRUCT(
    "ST_PARAMETER_BLIND_POSITION_TABLE"
)
    .addElement("uiSunElevation", new ARRAY(6, new UINT()))
    .addElement("uiBlindPosition", new ARRAY(6, new UINT()))
    .addElement("bValid", new BOOL())
    .addElement("byDummy", new BYTE());
