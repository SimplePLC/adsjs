import { BYTE, STRUCT, WORD } from "../../iecstruct";

export let ST_REG_DATA = new STRUCT("ST_REG_DATA")
    .addElement("wValue", new WORD())
    .addElement("byReg", new BYTE())
    .addElement("byReadWrite", new BYTE());
