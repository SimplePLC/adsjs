import { ARRAY, BYTE, STRUCT } from "../../iecstruct";
import * as config from "../global_config";
import { ST_DATA } from "./ST_DATA";

export const ST_PARAMETER_COMPLEX_LOGIC = new STRUCT(
    "ST_PARAMETER_COMPLEX_LOGIC"
)
    .addElement(
        "stDataIn",
        new ARRAY(config.uiMaxNoOfComplexLogicInputs, ST_DATA)
    )
    .addElement(
        "stDataOut",
        new ARRAY(config.uiMaxNoOfComplexLogicOutputs, ST_DATA)
    )
    .addElement("byLogicType", new BYTE())
    .addElement("bIsInUse", new BYTE());
