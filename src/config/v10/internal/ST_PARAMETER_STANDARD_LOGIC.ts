import { ARRAY, BYTE, STRUCT } from "../../iecstruct";
import * as config from "../global_config";
import { ST_DATA } from "./ST_DATA";

export const ST_PARAMETER_STANDARD_LOGIC = new STRUCT(
    "ST_PARAMETER_STANDARD_LOGIC"
)
    .addElement(
        "stDataIn",
        new ARRAY(config.uiMaxNoOfStandardLogicInputs, ST_DATA)
    )
    .addElement(
        "stDataOut",
        new ARRAY(config.uiMaxNoOfStandardLogicOutputs, ST_DATA)
    )
    .addElement("byLogicType", new BYTE())
    .addElement("bIsInUse", new BYTE());
