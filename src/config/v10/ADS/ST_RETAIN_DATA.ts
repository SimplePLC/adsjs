import * as iecstruct from "../../iecstruct";
import * as config from "../global_config";
import { ST_PARAMETER_BLIND } from "../internal/ST_PARAMETER_BLIND";
import { ST_PARAMETER_BUILDING_LOGIC } from "../internal/ST_PARAMETER_BUILDING_LOGIC";
import { ST_PARAMETER_COMPLEX_LOGIC } from "../internal/ST_PARAMETER_COMPLEX_LOGIC";
import { ST_PARAMETER_LIGHT } from "../internal/ST_PARAMETER_LIGHT";
import { ST_PARAMETER_STANDARD_LOGIC } from "../internal/ST_PARAMETER_STANDARD_LOGIC";

export const ST_RETAIN_DATA = new iecstruct.STRUCT("ST_RETAIN_DATA")
    .addElement(
        "aMerker",
        new iecstruct.ARRAY(config.uiMaxNoOfRetainMerker, new iecstruct.WORD())
    )
    .addElement(
        "stStandardLogicParameter",
        new iecstruct.ARRAY(
            config.uiMaxNoOfStandardLogics,
            ST_PARAMETER_STANDARD_LOGIC
        )
    )
    .addElement(
        "stComplexLogicParameter",
        new iecstruct.ARRAY(
            config.uiMaxNoOfComplexLogics,
            ST_PARAMETER_COMPLEX_LOGIC
        )
    )
    .addElement(
        "stBuildingLogicParameter",
        new iecstruct.ARRAY(
            config.uiMaxNoOfBuildingLogics,
            ST_PARAMETER_BUILDING_LOGIC
        )
    )
    .addElement("wSwitchTime", new iecstruct.WORD())
    .addElement("stLightParameter", ST_PARAMETER_LIGHT)
    .addElement("stBlindParameter", ST_PARAMETER_BLIND)
    .addElement("dtStartTime", new iecstruct.DT())
    .addElement("udiTerminalSum", new iecstruct.UDINT())
    .addElement("bRunBcLogicAfterReboot", new iecstruct.BOOL())
    .addElement("byDummy", new iecstruct.BYTE())
    .addElement("sLicenseKey", new iecstruct.STRING(15))
    .addElement("byTerminalConfigChangeCounter", new iecstruct.BYTE())
    .addElement("bDummy", new iecstruct.BOOL());
