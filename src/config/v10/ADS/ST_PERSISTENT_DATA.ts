import * as iecstruct from "../../iecstruct";
import * as config from "../global_config";
import { ST_REG_DATA } from "../internal/ST_REG_DATA";

export const ST_PERSISTENT_DATA = new iecstruct.STRUCT("ST_PERSISTENT_DATA")
    .addElement("dtDateTime", new iecstruct.DATE_AND_TIME())
    .addElement("uiMaxNoOfOutputs", new iecstruct.UINT())
    .addElement("Outputs", new iecstruct.ARRAY(512, new iecstruct.BYTE()))
    .addElement(
        "stAnalogChannelRegs",
        new iecstruct.ARRAY(
            config.uiMaxNoOfAnalogChannels - 1,
            new iecstruct.ARRAY(
                config.uiMaxNoOfPersistentAnalogReg,
                ST_REG_DATA
            )
        )
    )
    .addElement("uiNoOfDmxChannelsToAnalogChannels", new iecstruct.UINT())
    .addElement("iLongitude", new iecstruct.INT())
    .addElement("iLatitude", new iecstruct.INT())
    .addElement("iTimeZoneMinute", new iecstruct.INT())
    .addElement("bPersistentDataActive", new iecstruct.BOOL())
    .addElement("bDummy", new iecstruct.BOOL());
