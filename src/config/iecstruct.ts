abstract class Types {
    public byteLength: number;

    public asObject(buffer?, offset?) {
        const [buf, offs] = this.checkBuffer(buffer, offset);
        return this._asObject(buf, offs);
    }
    public fromObject(obj, buffer?, offset?) {
        const [buf, offs] = this.checkBuffer(buffer, offset);
        this._fromObject(obj, buf, offs);
        return buf;
    }
    public abstract graphql(name?: any): any;

    protected abstract _asObject(buffer, offset): any;
    protected abstract _fromObject(obj: any, buffer, offset): any;

    private checkBuffer(buffer?, offs = 0): [Buffer, number] {
        if (buffer !== undefined) {
            return [buffer, offs];
        } else {
            return [Buffer.alloc(this.byteLength), offs];
        }
    }
}
export class BOOL extends Types {
    constructor() {
        super();
        this.byteLength = 1;
    }
    public graphql() {
        return `Boolean`;
    }

    protected _asObject(buffer, offset) {
        return buffer.readUInt8(offset) !== 0;
    }

    protected _fromObject(obj: any, buffer, offset) {
        buffer.writeUInt8(obj ? 1 : 0, offset);
    }
}

export class STRUCT extends Types {
    public name: string;
    private elements = [];
    private elementNames = [];

    constructor(name: string) {
        super();
        this.name = name;
        this.byteLength = 0;
    }
    public graphql(list = [], sType = "type", prefix = "") {
        list.push(`${sType} ${prefix + this.name} {
${this.elements.reduce((acc, { name, type }) => {
            if (type instanceof STRUCT) {
                type.graphql(list, sType, prefix);
                return acc + `\t ${name}: ${prefix + type.name} \n`;
            } else if (type instanceof ARRAY) {
                return (
                    acc + `\t ${name}: ${type.graphql(list, sType, prefix)} \n`
                );
            } else {
                return acc + `\t ${name}: ${type.graphql()} \n`;
            }
        }, "")}}`);
        return list;
    }
    public addElement(name: string, type: any) {
        this.elementNames.push(name);
        if (type) {
            this.elements.push({
                byteLength: type.byteLength,
                name,
                type
            });
        }
        this.byteLength += type.byteLength;
        return this;
    }
    public offsetOffKey(name: string) {
        let offset = 0;
        for (let i = 0; i < this.elementNames.length; ++i) {
            if (this.elementNames[i] === name) {
                return offset;
            } else {
                offset += this.elements[i].byteLength;
            }
        }
        return null;
    }
    protected _asObject(buffer, offset) {
        const output = {};
        for (let i = 0; i < this.elementNames.length; ++i) {
            output[this.elementNames[i]] = this.elements[i].type.asObject(
                buffer,
                offset
            );
            offset += this.elements[i].byteLength;
        }
        return output;
    }
    protected _fromObject(obj: any, buffer, offset) {
        for (let i = 0; i < this.elementNames.length; ++i) {
            this.elements[i].type.fromObject(
                obj[this.elementNames[i]],
                buffer,
                offset
            );
            offset += this.elements[i].byteLength;
        }
    }
}

export class ARRAY extends Types {
    public length: number = 0;
    public type: Types;

    constructor(size: number, type: Types) {
        super();
        this.length = size;
        this.byteLength = size * type.byteLength;
        this.type = type;
    }
    public graphql(list = [], sType = "type", prefix = "") {
        if (this.type instanceof STRUCT || this.type instanceof ARRAY) {
            if (this.type instanceof STRUCT) {
                this.type.graphql(list, sType, prefix);
                return `[${prefix + this.type.name}]`;
            } else {
                return `[${this.type.graphql(list, sType, prefix)}]`;
            }
        } else {
            return `[${this.type.graphql()}]`;
        }
    }
    protected _asObject(buffer, offset) {
        const obj = [];
        for (let i = 0; i < this.length; ++i) {
            obj.push(this.type.asObject(buffer, offset));
            offset += this.type.byteLength;
        }
        return obj;
    }
    protected _fromObject(obj: any, buffer, offset) {
        const readlength = obj.length < this.length ? obj.length : this.length;
        for (let i = 0; i < readlength; ++i) {
            this.type.fromObject(obj[i], buffer, offset);
            offset += this.type.byteLength;
        }
    }
}
export class STRING extends Types {
    public size: number;
    constructor(size: number = 80) {
        super();
        this.size = size;
        this.byteLength = size + 1;
    }
    public graphql() {
        return `String`;
    }
    protected _asObject(buffer, offset) {
        return buffer
            .toString("ascii", offset, offset + this.size)
            .replace(/\0/g, "");
    }
    protected _fromObject(obj: any, buffer, offset) {
        const b = Buffer.from(obj).slice(0, this.size + 1);
        b[this.size] = 0;
        b.copy(buffer, offset);
    }
}

interface IEnum {
    [id: number]: string;
}
export class ENUM extends Types {
    public e: IEnum;
    constructor(type: IEnum) {
        super();
        this.e = type;
        this.byteLength = 2;
    }
    public graphql() {
        return `String`;
    }
    protected _asObject(buffer, offset) {
        return this.e[buffer.readUInt16LE(offset)];
    }
    protected _fromObject(obj: any, buffer, offset) {
        buffer.writeUInt16LE(this.e[obj], offset);
    }
}

abstract class INTEGER extends Types {
    public isUnsigned: boolean;
    public readString: string;
    public writeString: string;
    public graphqlString = "Int";
    public createStrings() {
        this.readString = `read${this.isUnsigned ? "U" : ""}Int${this
            .byteLength * 8}${this.byteLength === 1 ? "" : "LE"}`;
        this.writeString = `write${this.isUnsigned ? "U" : ""}Int${this
            .byteLength * 8}${this.byteLength === 1 ? "" : "LE"}`;
    }
    public graphql() {
        return this.graphqlString;
    }
    protected _asObject(buffer, offset) {
        if (this.readString === undefined) {
            this.createStrings();
        }
        return buffer[this.readString](offset);
    }
    protected _fromObject(obj: any, buffer, offset) {
        if (this.writeString === undefined) {
            this.createStrings();
        }
        buffer[this.writeString](obj, offset);
    }
}

export class BYTE extends INTEGER {
    constructor() {
        super();
        this.byteLength = 1;
        this.isUnsigned = true;
    }
}
export class WORD extends INTEGER {
    constructor() {
        super();
        this.byteLength = 2;
        this.isUnsigned = true;
    }
}
export class DWORD extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = true;
    }
}

export class USINT extends INTEGER {
    constructor() {
        super();
        this.byteLength = 1;
        this.isUnsigned = true;
    }
}

export class UINT extends INTEGER {
    constructor() {
        super();
        this.byteLength = 2;
        this.isUnsigned = true;
    }
}

export class UDINT extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = true;
    }
}

export class SINT extends INTEGER {
    constructor() {
        super();
        this.byteLength = 1;
        this.isUnsigned = false;
    }
}

export class INT extends INTEGER {
    constructor() {
        super();
        this.byteLength = 2;
        this.isUnsigned = false;
    }
}

export class DINT extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = false;
    }
}

export class REAL extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.readString = "readFloatLE";
        this.writeString = "writeFloatLE";
        this.graphqlString = "Float";
    }
}

export class LREAL extends INTEGER {
    constructor() {
        super();
        this.byteLength = 8;
        this.readString = "readDoubleLE";
        this.writeString = "writeDoubleLE";
        this.graphqlString = "Float";
    }
}

export class TIME extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = true;
    }
}

export class TOD extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = true;
    }
}

export class TIME_OF_DAY extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = true;
    }
}

export class DATE extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = true;
    }
}

export class DT extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = true;
    }
}

export class DATE_AND_TIME extends INTEGER {
    constructor() {
        super();
        this.byteLength = 4;
        this.isUnsigned = true;
    }
}
