import * as v10 from "./v10";
import * as v12 from "./v12";
export function getConfig(n: number) {
    switch (n) {
        case 10:
            return v10;
        case 12:
            return v12;
        default:
            return null;
    }
}

export { v12 as latest, iecstruct };
interface IAmsNetAddress {
    amsNetId: string;
    port: number;
}

interface IConfigMemoryAddress {
    RETAIN_DATA?: number;
    ST_ADS_TO_BC?: number;
    PERSISTENT_DATA?: number;
}

interface IPlcConfig {
    ams_address: IAmsNetAddress;
    config_version: number;
    ip_address: string;
    memory_addresses: IConfigMemoryAddress;
}

interface IConfig {
    ams_source: IAmsNetAddress;
    port: number;
    plcs: IPlcConfig[];
    timeout: number;
}
export const plcConfig: IConfig = require("../../config.json");