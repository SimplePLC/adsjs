import { GraphQLServer, PubSub } from "graphql-yoga";
import {plcConfig, getConfig} from "./config";
import { Client, createAdsServer } from "./ads";
import { createSchema } from "./handleConfig";
import * as plcHelper from "./plcHelper";
import * as httpProxy from "http-proxy";
import * as http from "http"

interface IPlcStructs {
    fromBC: any;
    persistent: any;
    retain: any;
    toBC: any;
}

export const pubsub = new PubSub();
export const mapOfConfVersion = new Map<string, number>();
export const mapOfPlc = new Map<string, IPlcStructs>();
export const mapOfClients = new Map<string, Client>();

const graphqls = createSchema();

const defaultServer = new GraphQLServer({
    resolvers: {
        Query: {
            listPLC(parent, args, context, info) {
                return plcConfig.plcs;
            }
        }
    },
    typeDefs: `
type PlcConn {
    amsNetId: String!
    port: Int!
}
type PLC {
    ams_address: PlcConn!
    config_version: Int!
}
type Query {
  listPLC: [PLC]!
}    
`
});

const proxy = httpProxy.createProxyServer({
    ws: true
});

const proxyServer = http.createServer((req,res)=>{
    const reqVersion = req.url.split("/").filter(v => v !== "");
    let version = (reqVersion.length < 1)?0:parseInt(reqVersion[0], 10);
    req.url = "/";
    if(isNaN(version)){
        version = 0;
    }
    proxy.web(req, res, {
        target: `http://127.0.0.1:${plcConfig.port+1+version}/`
    });
});

proxyServer.on('upgrade', (req, socket, head)=>{
    const reqVersion = req.url.split("/").filter(v => v !== "");
    const version = (reqVersion.length < 1)?0:parseInt(reqVersion[0], 10);
    req.url = "/";
    proxy.ws(req, socket, head, {
        target: `ws://127.0.0.1:${plcConfig.port+1+version}/`
    });
});

proxyServer.listen(plcConfig.port);


defaultServer.start({
    port: plcConfig.port+1,
    playground: false,
});
graphqls.forEach((schema, version) => {
    const server = new GraphQLServer(schema);
    server.start({
        playground: false,
        port: plcConfig.port+1 + version
    });
});
plcHelper.init();

createAdsServer(d => {
    const plc = mapOfPlc.get(d.source);
    const confVersion = mapOfConfVersion.get(d.source);
    const conf = getConfig(confVersion);
    if (plc && conf) {
        const byte = conf.ST_ADS_FROM_BC.fromObject(plc.fromBC);
        const res = byte.map((v, i) => {
            if (i < d.data.length) {
                return d.data[i];
            } else {
                return v;
            }
        });
        plc.fromBC = conf.ST_ADS_FROM_BC.asObject(res);
        if (
            JSON.stringify(plc.fromBC.aMerker) !==
            JSON.stringify(plc.retain.aMerker)
        ) {
            plc.retain.aMerker = plc.fromBC.aMerker;
            pubsub.publish(d.source + "ST_RETAIN_DATA", {
                ST_RETAIN_DATA: plc.retain
            });
        }
        mapOfPlc.set(d.source, plc);
        pubsub.publish(d.source + "ST_ADS_FROM_BC", {
            ST_ADS_FROM_BC: plc.fromBC
        });
    }
});
