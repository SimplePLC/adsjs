import { Client } from "./ads";
import { resolvers } from "./resolver";
import { schema } from "./schema";
import { mapOfPlc, mapOfConfVersion, mapOfClients } from "./index";
import {getConfig, plcConfig} from "./config";

export function AdsConnToString(conn: { amsNetId: string; port: number }) {
    return conn.amsNetId + ":" + conn.port;
}

const myGraphQLSchema = ({ Types, resolver }) => {
    return {
        resolvers: resolver,
        typeDefs: schema(Types)
    };
};

export const createSchema = () => {
    const resolver = resolvers();
    return plcConfig.plcs.reduce((acc, v) => {
        const conf = getConfig(v.config_version);
        if (conf !== null) {
            const client = new Client(
                v.ams_address,
                plcConfig.ams_source,
                v.ip_address
            );
            mapOfClients.set(AdsConnToString(v.ams_address), client);
            mapOfPlc.set(AdsConnToString(v.ams_address), {
                fromBC: conf.ST_ADS_FROM_BC.asObject(),
                persistent: conf.ST_PERSISTENT_DATA.asObject(),
                retain: conf.ST_RETAIN_DATA.asObject(),
                toBC: conf.ST_ADS_TO_BC.asObject()
            });
            mapOfConfVersion.set(
                AdsConnToString(v.ams_address),
                v.config_version
            );
            if (!acc.has(v.config_version)) {
                const list = [];
                conf.ST_ADS_FROM_BC.graphql(
                    list,
                    "type" /*`v${v.config_version}_`*/
                );
                conf.ST_ADS_TO_BC.graphql(
                    list,
                    "type" /*`v${v.config_version}_`*/
                );
                conf.ST_RETAIN_DATA.graphql(
                    list,
                    "type" /*`v${v.config_version}_`*/
                );
                conf.ST_PERSISTENT_DATA.graphql(
                    list,
                    "type" /*`v${v.config_version}_`*/
                );

                conf.ST_ADS_FROM_BC.graphql(
                    list,
                    "input",
                    /*`I_v${v.config_version}_`*/ "I_"
                );
                conf.ST_ADS_TO_BC.graphql(
                    list,
                    "input",
                    /*`I_v${v.config_version}_`*/ "I_"
                );
                conf.ST_RETAIN_DATA.graphql(
                    list,
                    "input",
                    /*`I_v${v.config_version}_`*/ "I_"
                );
                conf.ST_PERSISTENT_DATA.graphql(
                    list,
                    "input",
                    /*`I_v${v.config_version}_`*/ "I_"
                );

                acc.set(v.config_version, {
                    resolvers: resolver,
                    typeDefs: schema(Array.from(new Set(list)).join("\n"))
                });
            }
        }
        return acc;
    }, new Map());
};
